﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.UI.WebControls;
using WCF_Assignment.DbModels;

namespace WCF_Assignment
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public ResultItem SubmitItem(UploadedItem uploadedItem)
        {
            if (uploadedItem == null)
            {
                throw new ArgumentNullException("null");
            }
            var resultItem = new ResultItem();
            var VVServiceClient = new VVService.VilibidViliborSoapClient("VilibidViliborSoap");
            // TODO: Log
            using (var Db = new ApplicationDbContext())
            {
                try
                {
                    var agreement = Db.Agreements.Find(uploadedItem.Agreement);
                    var customer = Db.Customers.Find(uploadedItem.Customer);

                    //fetch data(base code) from web service
                    //TODO:additional checks for decimals/negative values etc.
                    //var currentBaseRateValue =  VVServiceClient.getLatestVilibRate(BaseRateCode);
                    //var newBaseRateValue =  VVServiceClient.getLatestVilibRate(NewBaseCode);
                    //var margins = agreement.Margin;
                    //var currentInterestRate = InterestRateHelper.GetCurrentInterestRate(currentBaseRateValue, margins);
                    //var newInterestRate = InterestRateHelper.GetNewInterestRate(newBaseRateValue, margins);
                    ////change new base code
                    //agreement.BaseRateCode = submitItemVm.NewBaseCode;
                    //var resultItemVm = new ResultItemVm()
                    //{
                    //    Agreement = agreement,
                    //    Customer = customer,
                    //    CurrentInterestRate = currentInterestRate,
                    //    NewInterestRate = newInterestRate,
                    //    DiffInterest = InterestRateHelper.GetInterestDifference(currentInterestRate, newInterestRate),
                    //};


                    //Db.SaveChanges();
                    
                }
                catch (Exception e)
                {
                    return null;
                }

            }
            return resultItem;
        }

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}
    }
}
