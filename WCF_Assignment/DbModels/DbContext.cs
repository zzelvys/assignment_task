﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WCF_Assignment.DbModels
{

        public class ApplicationDbContext : DbContext
        {

            public DbSet<Agreement> Agreements { get; set; }
            public DbSet<Customer> Customers { get; set; }

            public ApplicationDbContext()
                    : base("DefaultConnection")
            {
                Database.SetInitializer(new DbInitializer());
            }

            public static ApplicationDbContext Create()
            {
                return new ApplicationDbContext();
            }

        }
        public class DbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
        {
            protected override void Seed(ApplicationDbContext context)
            {
                IList<Customer> initCustomers = new List<Customer>();
                var cust1 = new Customer() { Name = "Goras", Surname = "Trusevičius", PersonalId = 67812203006 };
                var cust2 = new Customer() { Name = "Dange", Surname = "Kulkavičiūtė", PersonalId = 78706151287 };

                context.Customers.Add(cust1);
                context.Customers.Add(cust2);

                IList<Agreement> initAgreements = new List<Agreement>();
                //Goras
                initAgreements.Add(new Agreement() { Amount = 12000, BaseRateCode = "VILIBOR3m", Duration = 60, Margin = 1.6M, Customer = cust1 });
                //Dange
                initAgreements.Add(new Agreement() { Amount = 8000, BaseRateCode = "VILIBOR1y", Duration = 36, Margin = 2.2M, Customer = cust2 });
                initAgreements.Add(new Agreement() { Amount = 1000, BaseRateCode = "VILIBOR6m", Duration = 24, Margin = 1.85M, Customer = cust2 });

                foreach (Agreement aggr in initAgreements)
                    context.Agreements.Add(aggr);

                base.Seed(context);
            }
        }
}