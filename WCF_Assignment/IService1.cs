﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCF_Assignment
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        ResultItem SubmitItem(UploadedItem uploadedItem);
    }

    [DataContract]
    public class UploadedItem
    {
        [DataMember(IsRequired = true)]
        public string NewBaseRateCode { get; set; }
        [DataMember(IsRequired = true)]
        public Agreement Agreement  { get; set; }
        [DataMember(IsRequired = true)]
        public Customer Customer { get; set; }
    }
    [DataContract]
    public class Agreement
    {
        [DataMember(IsRequired = true)]
        public decimal Margin { get; set; }
        [DataMember(IsRequired = true)]
        public int Amount { get; set; }
        [DataMember(IsRequired = true)]
        public string BaseRateCode { get; set; }
        [DataMember(IsRequired = true)]
        public int Duration { get; set; }
    }
    [DataContract]
    public class Customer
    {
        [DataMember(IsRequired = true)]
        public long PersonalId { get; set; }
        [DataMember(IsRequired = true)]
        public string Name { get; set; }
        [DataMember(IsRequired = true)]
        public string Surname { get; set; }
        public string FullName => Name + " " + Surname;
    }

    //// Use a data contract as illustrated in the sample below to add composite types to service operations.
    ///         public virtual Agreement Agreement { get; set; }
    //public int CustomerId { get; set; }


    [DataContract]
    public class ResultItem
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public Agreement Agreement { get; set; }
        [DataMember]
        public string NewBaseCode { get; set; }
        [DataMember]
        public decimal CurrentInterestRate { get; set; }
        [DataMember]
        public decimal NewInterestRate { get; set; }
        [DataMember]
        public decimal DiffInterest { get; set; }
    }
    //[DataContract]
    //public class CompositeType
    //{
    //    bool boolValue = true;
    //    string stringValue = "Hello ";

    //    [DataMember]
    //    public bool BoolValue
    //    {
    //        get { return boolValue; }
    //        set { boolValue = value; }
    //    }

    //    [DataMember]
    //    public string StringValue
    //    {
    //        get { return stringValue; }
    //        set { stringValue = value; }
    //    }
    //}
}
