# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Programming Task

### How do I get set up? ###

* Summary of set up
  - Application was developed using Visual Studio 2015 so the easiest way to run application would be using VS2015 and simply running a new instance.
  - All packages are included but also can be restored during first Solution rebuild.
  - Web application and WCF service should be hosted on IIS/IISExpress.
  - Database : SQL 2014 compatibility level.
* Database Configuration
  - Configuration can be found in Web.config -> connectionStrings section node named "DefaultConnection". Configurable connection string, eg.:"Data Source=ZZ-PC\SQLEXPRESS;Initial Catalog=DbIdt;User Id=***;Password=***;MultipleActiveResultSets=true;"
  - Web application and WCF service can be configured identically.

### WCF/Web application ###
  - .NET framework 4.5.2, MVC5 using EntityFramework 6.1 with code first approach but without Migrations. ORM generates the best SQL code at no additional cost, quality of LINQ-to-Entities etc.

### What's missing? ###
  - WCF service can be hosted and called but nothing is returning, method is there but it needs some tweaks. I was also considering implementing security mode - "Transport With Message Credential" and message type "Client Credential Type", but it would have caused a big fuss.
  - Both applications lacks error handling / tests / calculation check procedures / web application needs visual design/CSS.
  - Customer drop down list - first time you have to re-select one item to make an AJAX call, because there is no initial value loaded.