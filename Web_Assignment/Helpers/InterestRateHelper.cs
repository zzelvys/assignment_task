﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Assignment.Helpers
{
    public class InterestRateHelper
    {
        public static decimal GetCurrentInterestRate(decimal baseRateValue, decimal margins)
        {
            return baseRateValue + margins;
        }

        public static decimal GetNewInterestRate(decimal baseRateValue, decimal margins)
        {
            return baseRateValue + margins;
        }
        public static decimal GetInterestDifference(decimal currentInterestRate,decimal newInterestRate)
        {
            return currentInterestRate - newInterestRate;
        }
    }
}