﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Web_Assignment.Helpers;
using Web_Assignment.Models;
using Web_Assignment.Models.ViewModels;

namespace Web_Assignment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult MainPage()
        {
            using (var Db = new ApplicationDbContext())
            {
                var submitItemVm = new SubmitItemVm();
                submitItemVm.CustomerModel = Db.Customers.ToList();

                return View(submitItemVm);
            }

        }
        [HttpPost]
        public async Task<ActionResult> MainPage([Bind(
                Include =
                    "NewBaseCode,SelectedAgreementId,SelectedCustomerId"
                )] SubmitItemVm submitItemVm)
        {

            if (!ModelState.IsValid)
            {
                return View("Error");
            }
            var VVServiceClient = new VVService.VilibidViliborSoapClient("VilibidViliborSoap");
            // TODO: Log
            using (var Db = new ApplicationDbContext())
            {
                try
                {
                    var agreement = Db.Agreements.Find(submitItemVm.SelectedAgreementId);
                    var customer = Db.Customers.Find(submitItemVm.SelectedCustomerId);

                    //fetch data(base code) from web service
                    //TODO:additional checks for decimals/negative values etc.
                    var currentBaseRateValue = await VVServiceClient.getLatestVilibRateAsync(agreement.BaseRateCode);
                    var newBaseRateValue = await VVServiceClient.getLatestVilibRateAsync(submitItemVm.NewBaseCode);
                    var margins = agreement.Margin;
                    var currentInterestRate = InterestRateHelper.GetCurrentInterestRate(currentBaseRateValue, margins);
                    var newInterestRate = InterestRateHelper.GetNewInterestRate(newBaseRateValue, margins);
                    //change new base code
                    agreement.BaseRateCode = submitItemVm.NewBaseCode;
                    var resultItemVm = new ResultItemVm()
                    {
                        Agreement = agreement,
                        Customer = customer,
                        CurrentInterestRate = currentInterestRate,
                        NewInterestRate = newInterestRate,
                        DiffInterest = InterestRateHelper.GetInterestDifference(currentInterestRate, newInterestRate), 
                    };


                    Db.SaveChanges();
                    return View("ResultView",resultItemVm);
                }
                catch (Exception e)
                {
                    return View("Error");
                }

            }
        }
        //Action result for ajax call
        [HttpPost]
        public ActionResult GetAgreementsByCustomerId(int customerId)
        {
            using (var Db = new ApplicationDbContext())
            {
                var agreements = Db.Agreements.Where(a => a.CustomerId == customerId).ToList();
                SelectList selectAgreements = new SelectList(agreements, "Id", "BaseRateCode", 0);
                return Json(selectAgreements);
            }

        }
    }
}