﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Assignment.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public long PersonalId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public ICollection<Agreement> Agreements { get; set; }
        public string FullName => Name + " " + Surname;
    }
}