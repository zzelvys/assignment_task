﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Assignment.Models.ViewModels
{
    //interest rate = [ base rate value] + [margin]
    public class ResultItemVm
    {
        public virtual Agreement Agreement { get; set; }
        public virtual Customer Customer { get; set; }
        public string NewBaseCode { get; set; }
        public decimal CurrentInterestRate { get; set; }
        public decimal NewInterestRate { get; set; }
        public decimal DiffInterest { get; set; }

//interest rate for current base rate(using base rate code currently stored with agreement)
//interest rate for new base rate
//difference between current and new interest rates
    }
}