﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_Assignment.Models.ViewModels
{
    public class SubmitItemVm
    {
        public int SelectedCustomerId { get; set; }
        public List<Customer> CustomerModel { get; set; }
        public int SelectedAgreementId { get; set; }
        public string NewBaseCode { get; set; }
    }
}