﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Assignment.Models
{
    public class Agreement
    {
        [Key]
        public int Id { get; set; }
        public decimal Margin { get; set; }
        public int Amount { get; set; }
        public string BaseRateCode { get; set; }
        public int Duration { get; set; }
        public virtual Customer Customer { get; set; }
        public int? CustomerId { get; set; }
    }
}